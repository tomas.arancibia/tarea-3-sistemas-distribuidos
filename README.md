# Tarea 3 Sistemas Distribuidos

Integrantes:
- Tomas Arancibia
- Benjamin Devia

## Instrucciones de montaje

Una vez descargado el repositorio, se debe acceder a la carpeta del mismo para comenzar con la instalación del sistema.
Luego de acceder a la carpeta en cuestión, se deben levantar los contenedores mediante el docker-compose utilizando el comando:

`sudo docker-compose up -d`

Tras ejecutar este comando, se deben haber iniciando 4 contenedores:
- node1
- node2
- node3
- api_rest

Donde node1, node2 y node3 son los nodos del cluster-cassandra, y api_rest simplemente es la API en donde se implementan los metodos solicitados.
Se recomienda verificar el estados de los contanedores mediante el comando:

`sudo docker ps`

Luego de verificar el correcto montaje de contenedores, **se debe esperar (1 minuto aprox) a que finalice la inicialización del cluster de cassandra**.
Ya pasado el tiempo de espera, **se debe verificar si el cluster se encuentra funcionando correctamente**. Para ello, se debe hacer una peticion de tipo GET a la API, la cual se ejecuta en el puerto 3000 del localhost:

`http://localhost:3000/cluster_status  [GET]`

Dicha petición debe retornar un json que indica que el cluster se encuentra óptimo para comenzar a trabajar:
```
{
    "Status_create": "Receta creada exitosamente para paciente existente"
}
```

**Si es que no se obtiene respuesta, se debe esperar un tiempo (1 minuto aprox nuevamente) y volver a intenar con la solicitud hasta que se obtenga dicha respuesta, ya que es la garantia de que el cluster cassandra este listo para utilizarce.***
Tras todo el proceder anterior, se tiene entonces al sistema listo para su funcionamiento.

## Instrucciones de funcionamiento

La API permite realizar 3 funciones principales solicitadas (sin contar la consulta al estado del cluster, que es solo para verificiar si se pueda comenzar a trabajar).

**1. Creacion de recetas:**
Esta función permite crear una nueva receta para los pacientes. Para utilizar el metodo, se debe realizar una peticion POST a la ruta /create, con la data incluida en el Body:

`http://localhost:3000/create [POST]`

Donde los datos enviandos en el Body deben ser, por ejemplo:
```
{
    "nombre": "Pedro",
    "apellido": "Picapiedras",
    "rut": "11.111.111-1",
    "email": "pedro.picapiedra@mail.udp.cl",
    "fecha_nacimiento": "01/01/1980",
    "comentario": "dolor de cabeza",
    "farmacos": "paracetamol",
    "doctor": "doctor strange"
}
```

Si es que el paciente es nuevo (que no se haya ingresado previamente), se obtendra la siguiente respuesta:

```
{
    "Status_create": "Paciente y receta creados exitosamente"
}
```

Mientras que si ya existe, se obtendra:

```
{
   "Status_create": "Receta creada exitosamente para paciente existente"
}
```

**2. Edicion de recetas:**
Este metodo nos permitirá editar una receta que ya haya sido ingresada previamente. Para utilizar el metodo, se debe realizar una peticion tipo POST a la ruta /edit, incluyendo la data en el Body:

`http://localhost:3000/edit [POST]`

Donde la data en el Body debe ser, por ejemplo:
```
{
    "id": 1,
    "comentario": "Dolor de hombros",
    "farmacos": "Ibuprofeno",
    "doctor": "doctor chapatin"
}
```

Si es que el ID de la receta existe, se obtendra la sigueinte respuesta:
```
{
    "Status_update": "Receta ID=1 actualizada exitosamente"
}
```

Si es que el ID no existe, se obtendra:
```
{
    "Status_update": "Falló",
    "Error": "Receta ID=3 no existe"
}
```

**3. Eliminación de recetas:**
Este metodo nos permitirá eliminar una receta que ya haya sido ingresada previamente. Para utilizar el metodo, se debe realizar una solicitud de tipo POST a la ruta /delete, especificando el ID de la receta en el Body:

`http://localhost:3000/delete [POST]`

Donde la data en el Body debe ser, por ejemplo:
```
{
    "id": 1
}
```

Si es que el ID de la receta existe, se obtendra la sigueinte respuesta: 
```
{
    "Status_delete": "Receta ID=2 eliminado exitosamente"
}
```
Si es que el ID no existe, se obtendra:
```
{
    "Status_delete": "Falló",
    "Error": "Receta ID=2 no existe"
}
```

## Preguntas
1. Explique la arquitectura que Cassandra maneja. Cuando se crea el cluster ¿Cómo los nodos se conectan? ¿Qué
ocurre cuando un cliente realiza una peticion a uno de los nodos? ¿Qué ocurre cuando uno de los nodos se desconecta?
¿La red generada entre los nodos siempre es eficiente? ¿Existe balanceo de carga?

Cassandra cuenta con una arquitectura P2P Estructurada, en donde los nodos se comunican constantemente utilizando el protocolo Goosip. 
Al crear cada nodo, se comienza a formar una estructura de tipo anillo, en donde cada nodo se conecta con sus adyacentes (sucesor y antecesor). Los ingresos al anillo pueden estar dados, ya sea de forma aleatoria, o simplemente buscando un orden en función de los keyspaces.
Cuando un cliente realiza una peticion a un nodo del cluster, este evalua si puede satisfacer dicha peticion (como por ejemplo, que poseea en su almacenamiento el keyspace solicitado), si es que no puede satisfacer la solicitud, deriva la peticion al siguiente nodo del anillo, quien deberá resolverla o bien derivarla al siguiente nodo, hasta que sea resulta. 
Al desconectarse un nodo, si existe un factor de replicación de sus keyspaces en otros nodos, no habria problema alguno, dado que hay copias de estos mismos en un numero determinado de nodos, por lo cual, la estructura se rearmaria, y continuaria el funcionamiento normal del cluster. Si es que el nodo se cae, y no hay replicas de sus keyspaces en los demas nodos, los datos se perderian, evitando ser accedidos por los clientes.
Ahora bien, la red generada entre los nodos no es simpre eficiente, dado que, como toda red P2P estructura, si existe un gran numero de nodos, y poca replicacion de datos, los tiempos de respuestas tienenden a aumentar significativamente en el peor de los casos. Ya que al aumentar la cantidad de nodos, se producirian mas redirecciones para encontrar el recurso solicitado, aumentando asi el tiempo de respuesta al cliente. En general va a depender entonces del tamaño de la red y del factor de replicacion utilizado.
Por otro lado, Cassandra sí utiliza balanceadores de carga. Esto para no saturar a los nodos del cluster, permitiendo agilizar los tiempos de respuesta y de procesamiento, ya que las peticiones (lecturas y escrituras) tienden a repartirse en los miembros de la estructura, evitando posibles cuellos de botellas.



2. Cassandra posee principalmente dos estrategias para mantener redundancia en la replicación de datos. ¿Cuáles son
estos? ¿Cuál es la ventaja de uno sobre otro? ¿Cuál utilizaría usted para en el caso actual y por qué? Justifique
apropiadamente su respuesta.

Cassandra tiene 2 estrategias de replicación, estas son:

- Simple Strategy
- Network Topology Strategy

Estas estrategias se diferencian principalmente en el número de datacenters donde funcionan de manera más óptima. Siendo Simple Strategy ideal para un único datacenter y Network Topology Strategy ideal para multiples data centers.
La principal ventaja de Network Topology Strategy es que se guardan los datos en puntos distintos, y eso entrega un nivel de tolerancia mucho mayor a Simple Strategy. Por el lado de Simple Strategy, la principal ventaja es que (asumiendo que se usa solo un datacenter), todos los datos se guardan de forma ordenada dentro del cluster.
En base a lo antes expuesto, y teniendo en cuenta que la solución no requiere de multiples datacenters, se recomienda el uso de una estrategia tipo Simple Strategy.

3. Teniendo en cuenta el contexto del problema ¿Usted cree que la solución propuesta es la correcta? ¿Qué ocurre
cuando se quiere escalar en la solución? ¿Qué mejoras implementaría? Oriente su respuesta hacia el Sharding (la
replicación/distribución de los datos) y comente una estrategia que podría seguir para ordenar los datos.

En efecto, nuestra solcuion es correcta debido a que cumple con el requerimiento de esta entrega. Ademas, genera un ecocistema distribuido, escalable y tolerante a fallos, ya que se utilizan factores de replicacion para garantizar la consistencia de los datos y la disponibilidad de estos. Si deseamos que el sistema sea mas escalable, basta con agregar mas nodos al cluster (lo cual es sumamente sencillo bajo el contexto de docker), y aumentar el factor de replicacion para disponibilizar aun mas los datos (distribuirlos en los nodos), que permitirian reducir los tiempos de respuesta cuando se produzcan posibles cuellos de botella.
 


