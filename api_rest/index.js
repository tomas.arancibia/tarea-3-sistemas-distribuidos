const express = require('express');
const app = express();
const port = 3000;
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const cassandra = require('cassandra-driver');
const authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', 'password123');
const contactPoints = ['node1','node2', 'node3'];

const space1 = new cassandra.Client({contactPoints: contactPoints, authProvider: authProvider, localDataCenter: 'datacenter1', keyspace: 'space1'});
const space2 = new cassandra.Client({contactPoints: contactPoints, authProvider: authProvider, localDataCenter: 'datacenter1', keyspace: 'space2'});

var id_paciente = 1;
var id_recetas = 1;

app.get('/cluster_status', async(req, res) => {
    const res1 = await space1.connect();
    res.json({'Status_Cluster': 'Cluster Cassandra operativo y listo para usar!'});
});

app.post('/create', (req, res) => {
  const query = "SELECT id FROM paciente WHERE rut='"+req.body.rut+"' ALLOW FILTERING";
  space1.execute(query)
    .then(result => {
        if(result.rows.length > 0){
          const id = result.rows[0].id;
          
          const query4 = "INSERT INTO receta (id, id_paciente, comentario, farmaco, doctor) VALUES ("+id_recetas+", "+id+", '"+req.body.comentario+"', '"+req.body.farmacos+"', '"+req.body.doctor+"')";
          space2.execute(query4)
          .then(result => {
              if(result){
                id_recetas++;
                res.json({'Status_create': 'Receta creada exitosamente para paciente existente'});
              }
          })
          .catch((err) => {
            console.log('Error:', err);
            res.json({'Status_create': 'Falló', 'Error': err});
          });
                    
        }

        else{
          const query1 = "INSERT INTO paciente (id, nombre, apellido, rut, email, fecha_nacimiento) VALUES ("+id_paciente+", '"+req.body.nombre+"', '"+req.body.apellido+"', '"+req.body.rut+"', '"+req.body.email+"', '"+req.body.fecha_nacimiento+"');";
          const query2 = "INSERT INTO receta (id, id_paciente, comentario, farmaco, doctor) VALUES ("+id_recetas+", "+id_paciente+", '"+req.body.comentario+"', '"+req.body.farmacos+"', '"+req.body.doctor+"')";

          space1.execute(query1)
          .then(result => {

            (async() => {
              await Proceder_2();
            })();

          })
          .catch((err) => {
            console.log('Error:', err);
            res.json({'Status_create': 'Falló', 'Error': err});
          });

          async function Proceder_2(){
            space2.execute(query2)
            .then(result => {
              id_paciente++;
              id_recetas++;
              res.json({'Status_create': 'Paciente y receta creados exitosamente'});
            })
            .catch((err) => {
              console.log('Error:', err);
              res.json({'Status_create': 'Falló', 'Error': err});
            });
          };

        }
    })
    .catch((err) => {
      console.log('Error:', err);
      res.json({'Status_create': 'Falló', 'Error': err});
    });
});

app.post('/edit', (req, res) => {
  const query1 = `SELECT * FROM receta WHERE id=${req.body.id};`;
  space2.execute(query1)
    .then(result => {
        if(result.rows.length > 0){
          const query2 = "UPDATE receta SET comentario='"+req.body.comentario+"', doctor='"+req.body.doctor+"', farmaco='"+req.body.farmacos+"' WHERE id="+req.body.id+";";
          space2.execute(query2)
            .then(result => {
                res.json({'Status_update': `Receta ID=${req.body.id} actualizada exitosamente`});
            })
            .catch((err) => {
              console.log('Error:', err);
              res.json({'Status_update': 'Falló', 'Error': err});
            });
        }
        else{
          res.json({'Status_update': 'Falló', 'Error': `Receta ID=${req.body.id} no existe`});
        }
    })
    .catch((err) => {
      console.log('Error:', err);
      res.json({'Status_update': 'Falló', 'Error': err});
    });
});

app.post('/delete', (req, res) => {
  const query = "SELECT * FROM receta WHERE id="+req.body.id+";";
  space2.execute(query)
  .then(result => {
    if(result.rows.length > 0){
      const query1 = "DELETE FROM receta WHERE id="+req.body.id+";";
      space2.execute(query1)
      .then(result => {
        res.json({'Status_delete': `Receta ID=${req.body.id} eliminado exitosamente`});
      })
      .catch((err) => {
        console.log('Error:', err);
        res.json({'Status_delete': 'Falló', 'Error': err});
      });
    } 
    else{
      res.json({'Status_delete': 'Falló', 'Error': `Receta ID=${req.body.id} no existe`});
    }
  })
  .catch((err) => {
    console.log('Error:', err);
    res.json({'Status_delete': 'Falló', 'Error': err});
  });
});


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
